<?php
/**
 * @file
 * Handles the admin forms of the module.
 */

/**
 * Callback of the main settings form module
 */
function pca_settings_form($form, &$form_state) {
  $form['license'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('License settings'),
    '#description'    => t('Account settings.'),
    '#collapsible'    => TRUE,
    '#collapsed'      => FALSE,
  );
  $form['license']['pca_licence'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Web service Key'),
    '#default_value'  => variable_get('pca_licence', ''),
  );
  $form['license']['pca_url'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Postcodeanywhere URL'),
    '#default_value'  => variable_get('pca_url', 'http://services.postcodeanywhere.co.uk/inline.aspx'),
  );
  $form['form_ids_overview'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Form ids with postcodeanywhere'),
    '#description'    => t('Form ids for setup postcodeanywhere'),
    '#collapsible'    => TRUE,
    '#collapsed'      => FALSE,
  );

  $pca_form_ids = _get_pca_form_ids();

  $form['form_ids_overview']['pca_form_list'] = array(
    '#theme' => 'pca_form_list_decorations',
    '#tree'  => TRUE,
  );

  foreach ($pca_form_ids as $pca_form_id => $pca_form_data) {
    $form['form_ids_overview']['pca_form_list']['pca_form'][$pca_form_id] = array();
    $form['form_ids_overview']['pca_form_list']['pca_form'][$pca_form_id]['fid'] = array(
      '#markup' => $pca_form_id,
    );
    $form['form_ids_overview']['pca_form_list']['pca_form'][$pca_form_id]['status'] = array(
      '#markup' => ($pca_form_data['status'])? 'Enabled' : 'Disabled',
    );
    $form['form_ids_overview']['pca_form_list']['pca_form'][$pca_form_id]['op_links'] = array(
      '#markup' => $pca_form_data['form_op_links'],
    );
  }

  // Form items for new form_id.
  $form['form_ids_overview']['pca_form_list']['pca_new_form'] = array();
  // Textfield for form_id.
  $form['form_ids_overview']['pca_form_list']['pca_new_form']['fid'] = array(
    '#type' => 'textfield',
    '#size' => 16,
  );
  $form['other_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form Settings'),
    '#description' => t('Form Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['other_settings']['postcodeanywhere_id_country_uk_value'] = array(
    '#type' => 'textfield',
    '#title' => t('GB Country Key'),
    '#description' => t('Type here the value for uk in country select'),
    '#default_value' => variable_get('postcodeanywhere_id_country_uk_value', 'GB'),
  );
  $form['#submit'][] = 'pca_setting_form_submit';

  return system_settings_form($form);
}

/**
 * Callback of the form_id settings
 */
function pca_form_id_edit($form, &$form_state, $fid){
  $add_form = array();
  $add_form = _pca_form($form, $form_state, $fid);
  return $add_form;
}

/**
 * Implements of hook_form_submit().
 */
function pca_form_id_edit_submit($form, &$form_state){
  $pca_form_id = $form_state['values']['pca_new_form_options']['pca_form_id'];
  $pca_form_id_values = array('status' => $form_state['values']['pca_new_form_options']['pca_form_status']);
  _pca_db_merge($pca_form_id, $pca_form_id_values);
  $pca_form_id_text_fields = $form_state['values']['pca_form_text_fields'];
  _pca_form_id_edit_db_merge($pca_form_id, $pca_form_id_text_fields);
}

/**
 * Helper database submit function of pca_form_id_edit form
 */
function _pca_form_id_edit_db_merge($form_id, $form_values) {
  foreach($form_values as $fkey => $fvalue){
    db_merge('pca_form_fields')
      ->key(array('field_id' => $fkey))
      ->fields(array(
        'form_id' => $form_id,
        'status' => $fvalue,
      ))
      ->execute();
  }
}

/**
 * Creates form_id settings form
 *
 * @see pca_form_id_edit()
 */
function _pca_form($form, &$form_state, $fid) {
  $form = array();
  $default_values = _get_pca_form_id($fid);
  $active = array(0 => t('Disabled'), 1 => t('Enabled'));

  $form['pca_go_top_settings_page'] = array(
    '#markup' => l('To main settings page', 'admin/config/pca'),
  );
  $form['pca_new_form_options'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Main form settings'),
  );
  $form['pca_new_form_options']['pca_form_id'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Form ID'),
    '#required' => TRUE,
    '#default_value' => $default_values['form_id'],
  );
  $form['pca_new_form_options']['pca_form_status'] = array(
    '#type'    => 'radios',
    '#title'   => t('Form status'),
    '#options' => $active,
    '#default_value' => $default_values['status'],
  );
  $form['pca_form_text_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form fields'),
  );

  $form_elements = _get_form_textfields($default_values['form_id']);

  $form_fields_options = array();
  $form['#tree'] = TRUE;
  foreach ($form_elements as $element) {
    $form['pca_form_text_fields'][$element['#name']] = array(
      '#type' => 'checkbox',
      '#title' => t($element['#title']),
      '#default_value' => $element['#default'],
    );
  }
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function get_pca_fields_types() {
  $pca_fields_types = array(
    'department'      => 'Department',
    'company'         => 'Company',
    'sub_building'    => 'SubBuilding',
    'building_number' => 'BuildingNumber',
    'building_name'   => 'BuildingName',
    'secondary_street' => 'SecondaryStreet',
    'street'          => 'Street',
    'block'           => 'Block',
    'neighbourhood'   => 'Neighbourhood',
    'district'        => 'District',
    'city'            => 'City',
    'line1' => 'Line1',
    'line2' => 'Line2',
    'line3' => 'Line3',
    'line4' => 'Line4',
    'line5' => 'Line5',
    'admin_area_name' => 'AdminAreaName',
    'admin_area_code' => 'AdminAreaCode',
    'province' => 'Province',
    'province_name' => 'ProvinceName',
    'province_code' => 'ProvinceCode',
    'postal_code' => 'PostalCode',
    'country_name' => 'CountryName',
    'country_iso2' => 'CountryIso2',
    'country_iso3' => 'CountryIso3',
    'country_iso_number' => 'CountryIsoNumber',
    'sorting_number1' => 'SortingNumber1',
    'sorting_number2' => 'SortingNumber2',
    'barcode' => 'Barcode',
    'po_box_number' => 'POBoxNumber',
    'label' => 'Label',
    'type' => 'Type',
    'data_level' => 'DataLevel',
  );
}

/**
 * The Main function to get all text fields of the form by form_id
 *
 * @see _pca_form()
 */
function _get_form_textfields ($form_id){
  $form_state = form_state_defaults();
  $form = drupal_build_form($form_id, $form_state);
  $elements = array();
  $text_elements = get_textfields_elements($form, $elements);
  return $text_elements;
}

/**
 * Helper function to get all text fields of the form
 *
 * @see _get_form_textfields().
 */
function get_textfields_elements(&$element, &$elements) {
  if ((isset($element['#type'])) && (in_array($element['#type'], array('textfield')))) {
    $name = (isset($element['#field_name']))? $element['#field_name'] : $element['#name'];
    $elements[] = array(
      '#title' => $element['#title'],
      '#name' => $name,
      '#default' => _get_default_values_textfields($name),
    );
  }
  foreach (element_children($element) as $key) {
    get_textfields_elements($element[$key], $elements);
  }
  return $elements;
}


/**
 * Get field status in pca_form_id_edit form
 *
 * @param $field_name
 *  A string representing the name of the field
 * @return
 *  Status (1 or 0) of the field in pca_form_id_edit form
 */
function _get_default_values_textfields($field_name){
  $def_value = db_select('pca_form_fields', 'pff')
    ->fields('pff', array('status'))
    ->condition('field_id', $field_name)
    ->execute()
    ->fetchField();
  return $def_value;
}

/**
 * Get text fields list with the form ids where postcodeanywhere service can be activated
 */
function _get_pca_form_ids_list(&$form, $fieldset) {
  $pca_form_ids = _get_pca_form_ids();

  $form[$fieldset]['pca_form_list'] = array(
    '#theme' => 'pca_form_list_decorations',
    '#tree'  => TRUE,
  );

  foreach ($pca_form_ids as $pca_form_id => $pca_form_data) {
    $form_default_ids[$pca_form_id] = (boolean)$pca_form_data['status'];
    $form[$fieldset]['pca_form_list']['pca_form'][$pca_form_id] = array();
    $form[$fieldset]['pca_form_list']['pca_form'][$pca_form_id]['fid'] = array(
      '#markup' => $pca_form_id,
    );
    $form[$fieldset]['pca_form_list']['pca_form'][$pca_form_id]['status'] = array(
      '#markup' => ($pca_form_data['status'])? 'Enabled' : 'Disabled',
    );
    $form[$fieldset]['pca_form_list']['pca_form'][$pca_form_id]['op_links'] = array(
      '#markup' => $pca_form_data['form_op_links'],
    );
  }

  // Form items for new form_id.
  $form[$fieldset]['pca_form_list']['pca_new_form'] = array();
  // Textfield for form_id.
  $form[$fieldset]['pca_form_list']['pca_new_form']['fid'] = array(
    '#type' => 'textfield',
    '#size' => 16,
  );

  return $form;
}

/**
 * Theme callback for the main pca form
 */
function theme_pca_form_list_decorations($variables) {
  $form = $variables['form'];
  $header = array('fid' => t('Form id'), 'status' => t('Status') , 'op_links' => t('Operations'));
  $rows = array();
  // Existing form ids.
  foreach (element_children($form['pca_form']) as $key) {
    $row = array();
    $row[] = drupal_render($form['pca_form'][$key]['fid']);
    $row[] = drupal_render($form['pca_form'][$key]['status']);
    $row[] = drupal_render($form['pca_form'][$key]['op_links']);
    $rows[] = $row;
  }
  // For new form id.
  $row = array();
  $row[] = drupal_render($form['pca_new_form']['fid']);
  $row[] = '';
  $row[] = '';
  $rows[] = $row;

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}

/**
 * Submit function for pca_setting_form.
 */
function pca_setting_form_submit($form, &$form_state) {
  $fid_new = $form_state['values']['pca_form_list']['pca_new_form']['fid'];
  if ((isset($fid_new)) && !empty($fid_new)) {
    _pca_db_merge($fid_new, array('status' => 1));
  }
}


/**
 * Helper database submit function
 */
function _pca_db_merge($form_id = NULL, $form_values) {
  db_merge('pca_forms')
    ->key(array('form_id' => $form_id))
    ->fields(array(
      'status' => $form_values['status'],
      'module' => 'pca',
    ))
    ->execute();
}

/**
 * Get all form ids from database
 */
function _get_pca_form_ids() {
  $query = db_select('pca_forms', 'p_forms')
    ->fields('p_forms')
    ->execute();
  while($record = $query->fetchAssoc()) {
    $result[$record['form_id']] = array(
      'form_id' => $record['form_id'],
      'form_op_links' => l('Edit', 'admin/config/pca/form/' . $record['form_id'].'/edit').'&nbsp;&nbsp;'.l('Delete', 'admin/config/pca/form/' . $record['form_id'].'/delete'),
      'status' => $record['status'],
    );
  }
  return $result;
}

/**
 * Get pca form setting from database by form id
 */
function _get_pca_form_id($fid) {
  $query = db_select('pca_forms', 'p_forms')
    ->fields('p_forms', array('form_id', 'status'))
    ->condition('form_id', $fid)
    ->execute()
    ->fetchAssoc();
  return $query;
}

/**
 * Remove form id from settings table
 */
function pca_form_id_delete($fid) {
  db_delete('pca_forms')
    ->condition('form_id', $fid)
    ->execute();
  db_delete('pca_form_fields')
    ->condition('form_id', $fid)
    ->execute();
  drupal_goto('admin/config/pca');
}